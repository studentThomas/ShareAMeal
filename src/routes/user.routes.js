const express = require('express');
const router = express.Router();
const logger = require('../util/logger').logger;
const port = 3000;

router.use(express.json());

let database = require('../util/inmem-db');

let index = database.users.length;

//use case 201
router.post('/api/register', (req, res) => {
  logger.info('Registering user called');
  const user = {
    id: index++,
    emailAdress: req.body.emailAdress,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    isActive: false,
    phoneNumber: req.body.phoneNumber,
    city: req.body.city,
    street: req.body.street
  };

  const isEmailUnique = database.users.every(
    (user) => user.emailAdress !== req.body.emailAdress
  );

  if (!isEmailUnique) {
    logger.warn(`User with email ${req.body.emailAdress} already exists`);
    res.status(400).send({
      status: 400,
      message: `User with email ${req.body.emailAdress} already exists`
    });
    return;
  }

  database.users.push(user);
  res.send({
    status: 200,
    message: `User with id ${user.id} is added`,
    data: user
  });
});

//use case 202
router.get('/api/user', (req, res) => {
  logger.info('Requesting overview of users called');
  const filters = {
    firstName: req.query.field1,
    emailAdress: req.query.field2
  };

  if (Object.values(filters).some((filter) => filter !== undefined)) {
    const filteredDatabase = database.users.filter((user) => {
      return Object.entries(filters).every(([property, value]) => {
        return value === undefined || user[property] === value;
      });
    });

    logger.info('Filtered users');
    res.send({
      status: 200,
      message: 'Filtered users',
      data: filteredDatabase
    });
  } else {
    logger.info('All users');
    res.send({
      status: 200,
      message: 'All users',
      data: database.users
    });
  }
});

//use case 203
router.get('/api/user/profile', (req, res) => {
  logger.info('Requesting user profile called');
  const user = database.users.find((user) => user.id === 0);

  res.send({
    status: 200,
    message: 'User profile',
    data: user
  });
});

//use case 204
router.get('/api/user/:userId', (req, res) => {
  logger.info('Requesting user details called');
  const userId = req.params.userId;

  const user = database.users.find((user) => user.id === parseInt(userId));

  if (user) {
    res.send({
      status: 200,
      message: 'User details',
      data: user
    });
  } else {
    logger.warn(`User with id ${userId} not found`);
    res.send({
      status: 404,
      message: `User with id ${userId} not found`
    });
  }
});

//use case 205
router.put('/api/user/:userId', (req, res) => {
  logger.info('Updating user called');
  const userId = req.params.userId;
  const updatedUser = req.body;

  const user = database.users.find((user) => user.id === parseInt(userId));

  if (user) {
    Object.assign(user, updatedUser);
    res.send({
      status: 200,
      message: `User with id ${userId} is updated`,
      data: user
    });
  } else {
    logger.warn(`User with id ${userId} not found`);
    res.send({
      status: 404,
      message: `User with id ${userId} not found`
    });
  }
});

//use case 206
router.delete('/api/user/:userId', (req, res) => {
  logger.info('Deleting user called');
  const userId = req.params.userId;

  const index = database.users.findIndex(
    (user) => user.id === parseInt(userId)
  );

  if (index !== -1) {
    database.users.splice(index, 1);
    res.send({
      status: 200,
      message: 'User with id ' + userId + ' is deleted'
    });
  } else {
    logger.warn(`User with id ${userId} not found`);
    res.send({
      status: 404,
      message: 'User not found'
    });
  }
});

module.exports = router;
