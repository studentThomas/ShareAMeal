let database = {
  users: [
    {
      id: 0,
      emailAdress: 'hvd@server.nl',
      password: '12345',
      firstName: 'Hendrik',
      lastName: 'van Dam',
      isActive: false,
      phoneNumber: '0612345678',
      city: 'Breda',
      street: 'Lovensdijkstraat'
    },
    {
      id: 1,
      emailAdress: 'm@server.nl',
      password: '54321',
      firstName: 'Marieke',
      lastName: 'Jansen',
      isActive: true,
      phoneNumber: '0698765432',
      city: 'Breda',
      street: 'Hogeschoollaan'
    }
  ]
};

module.exports = database;
