const express = require('express');
const router = require('./src/routes/user.routes');
const logger = require('./src/util/logger').logger;
const app = express();
const port = 3000;

app.use(express.json());

app.use('*', (req, res, next) => {
  const method = req.method;
  logger.trace(`Methode ${method} is aangeroepen`);
  next();
});

app.use(router);

app.use('*', (req, res) => {
  logger.warn('Endpoint not found');
  res.status(404).json({
    status: 404,
    message: 'Endpoint not found',
    data: {}
  });
});

app.listen(port, () => {
  logger.info(`Example app listening on port ${port}`);
});

module.exports = app;
