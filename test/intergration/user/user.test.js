const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
require('tracer').setLevel('error');
const server = require('../../../index');

chai.should();
chai.use(chaiHttp);

describe('UC-201 Register as new user', () => {
  it('TC-201-5 - User registered successfully', (done) => {
    chai
      .request(server)
      .post('/api/register')
      .send({
        emailAdress: 'testuser@test.com',
        password: 'password',
        firstName: 'Test',
        lastName: 'User',
        isActive: false,
        phoneNumber: '0612345678',
        city: 'Amsterdam',
        street: 'Street'
      })
      .end((err, res) => {
        res.body.should.be.an('object');

        let { data, message, status } = res.body;

        data.id.should.equal(2);
        data.emailAdress.should.equal('testuser@test.com');
        data.password.should.equal('password');
        data.firstName.should.equal('Test');
        data.lastName.should.equal('User');
        data.isActive.should.be.false;
        data.phoneNumber.should.equal('0612345678');
        data.city.should.equal('Amsterdam');
        data.street.should.equal('Street');

        message.should.equal('User with id 2 is added');
        status.should.equal(200);

        done();
      });
  });
});

describe('UC-202 Requesting an overview of users', () => {
  it('TC-202-1 - Shows all users (minimum 2)', (done) => {
    chai
      .request(server)
      .get('/api/user')
      .end((err, res) => {
        res.body.should.be.an('object');

        let { data, message, status } = res.body;

        data.should.be.an('array');
        data.length.should.be.greaterThan(2);

        status.should.equal(200);
        message.should.equal('All users');

        done();
      });
  });
});

describe('UC-203 Retrieval of user profile', () => {
  it('TC-203-2 - User is logged in with valid token', (done) => {
    chai
      .request(server)
      .get('/api/user/profile')
      .end((err, res) => {
        res.body.should.be.an('object');

        let { data, message, status } = res.body;

        data.id.should.equal(0);
        data.emailAdress.should.equal('hvd@server.nl');
        data.password.should.equal('12345');
        data.firstName.should.equal('Hendrik');
        data.lastName.should.equal('van Dam');
        data.isActive.should.be.false;
        data.phoneNumber.should.equal('0612345678');
        data.city.should.equal('Breda');
        data.street.should.equal('Lovensdijkstraat');

        status.should.equal(200);
        message.should.equal('User profile');

        done();
      });
  });
});

describe('UC-204 Retrieval of user', () => {
  it('TC-204-3 - User id exists', (done) => {
    chai
      .request(server)
      .get('/api/user/0')
      .end((err, res) => {
        res.body.should.be.an('object');

        let { data, message, status } = res.body;

        data.id.should.equal(0);
        data.emailAdress.should.equal('hvd@server.nl');
        data.password.should.equal('12345');
        data.firstName.should.equal('Hendrik');
        data.lastName.should.equal('van Dam');
        data.isActive.should.be.false;
        data.phoneNumber.should.equal('0612345678');
        data.city.should.equal('Breda');
        data.street.should.equal('Lovensdijkstraat');

        status.should.equal(200);
        message.should.equal('User details');

        done();
      });
  });
});

describe('UC-206 Remove user', () => {
  it('TC-206-4 - User deleted successfully', (done) => {
    chai
      .request(server)
      .delete('/api/user/1')
      .end((err, res) => {
        res.body.should.be.an('object');

        let { message, status } = res.body;

        status.should.equal(200);
        message.should.equal('User with id 1 is deleted');

        done();
      });
  });
});
